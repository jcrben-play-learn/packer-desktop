#!/bin/bash -e
export DEBIAN_FRONTEND=noninteractive # per https://serverfault.com/q/500764/309584
# apt-get -y -qq update && apt-get -y dist-upgrade

# TODO: properly integrate with my bashrc
# default in /etc/default/locale is en.US - why is UTF-8 not the default?

# TODO: move this into the base box! tricky because I have to trigger a reboot
# region vbox guest additions per https://askubuntu.com/a/394368/457417
# modprobe -r vboxguest
# sed -i '$ d' /etc/modprobe.d/blacklist.conf

# wget -nv "https://www.virtualbox.org/download/testcase/VBoxGuestAdditions_5.2.7-120528.iso"
# mkdir /media/GuestAdditionsISO
# mount -o loop ./VBoxGuestAdditions_5.2.7-120528.iso /media/GuestAdditionsISO
# cd /media/GuestAdditionsISO
# ./VBoxLinuxAdditions.run install

# gpasswd -a ben vboxsf
# endregion vbox


# TODO: figure out why this throws an error exit code
# one of those cases where double-quotes is important - othewise lines are gone
# eval "$BASHRC_APPEND"
# echo "$BASHRC_APPEND" | source /dev/stdin
# source <(echo "$BASHRC_APPEND")

export PACKER_BUILD=1
export LANG=en_US.UTF-8
export MY_VIRTUALBOX=1
source /media/sf_dotfiles/startup_shell/pathmunge.sh
pathmunge /home/ben/.local/bin/
pathmunge "$HOME"/.pyenv/bin
source /media/sf_dotfiles/bashrc

#  2>&1 | tee -a some_file
cat << \EOF >> /home/ben/.bashrc
export LANG=en_US.UTF-8
export MY_VIRTUALBOX=1
source /media/sf_dotfiles/startup_shell/pathmunge.sh
pathmunge /home/ben/.local/bin/
pathmunge /home/ben/.pyenv/bin
source /media/sf_dotfiles/bashrc
EOF

cat << \EOF >> /home/ben/.profile
export PATH=/home/ben/.config/i3/scripts:$PATH 
EOF

# TODO: this was adding quotes...
# BASHRC_APPEND=$(cat <<\EOF
# export LANG=en_US.UTF-8
# export MY_VIRTUALBOX=1
# source /media/sf_dotfiles/startup_shell/pathmunge.sh
# pathmunge /home/ben/.local/bin
# pathmunge "$HOME"/.pyenv/bin
# source /media/sf_dotfiles/bashrc
# EOF
# )
# setup_env() {
# cat << EOF >> /home/ben/.bashrc
# "$BASHRC_APPEND"
# EOF
# }
# setup_env

# TODO: consider moving this into this repo
# has to come after the VBoxManage commands
# mv /etc/sddm.conf /etc/sddm.conf.backup
echo "checking sf_dotfiles"
ls -la /media/sf_dotfiles
ln -s /media/sf_dotfiles/linux/sddm/sddm.conf /etc/sddm.conf
ln -s /media/sf_dotfiles/linux/sddm /etc/sddm
cp -R /media/sf_dotfiles/linux/sddm /etc/sddm.nolink
# permissions seems to be quite important@
# below two are very important!!
chmod 755 /media/sf_dotfiles/linux/sddm/Xsession
chmod 755 /media/sf_dotfiles/linux/sddm/Xsetup
chmod 755 /etc/sddm.nolink
mv /home/ben/.config /home/ben/.config.backup
ln -s /media/sf_dotfiles/linux/config /home/ben/.config

# mywayface
# apt -y python3-pip
# pip3 install meson
# apt -y install pkg-config
