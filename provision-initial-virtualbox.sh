#!/bin/bash -e

modprobe -r vboxguest
sed -i '$ d' /etc/modprobe.d/blacklist.conf

wget -nv "https://www.virtualbox.org/download/testcase/VBoxGuestAdditions_5.2.7-120528.iso"
mkdir /media/GuestAdditionsISO
mount -o loop ./VBoxGuestAdditions_5.2.7-120528.iso /media/GuestAdditionsISO
cd /media/GuestAdditionsISO
./VBoxLinuxAdditions.run install

gpasswd -a ben vboxsf